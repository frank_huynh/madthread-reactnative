import React, {Component} from "react";
import {Provider} from "react-redux";
import configureStore from './configureStore'
import AppNavigator from "./AppNavigator";
import {PersistGate} from 'redux-persist/integration/react';
import {Image, View} from 'react-native'
import {colors} from './utils/themeConfig'

const { persistor, store } = configureStore();
const madThreadIc = require('./assets/ic_app_icon.png')

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={
                    <View style={{flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: colors.mainWhite}}>
                        <Image source={madThreadIc}/>
                   </View>     
                } persistor={persistor}>
                    <AppNavigator/>
                </PersistGate>
            </Provider>
        )
    }
}
