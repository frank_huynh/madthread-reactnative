import React, {Component} from 'react';
import {connect} from "react-redux";
import {BackHandler } from 'react-native'
import {NavigationActions} from 'react-navigation'
import {ScreenStack} from './registerScreens';
import NavigationService from './NavigationService'

class AppNavigator extends Component {

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPress.bind(this));
    }
  
    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.onBackPress.bind(this));
    }
  
    onBackPress() {
      const { dispatch, nav } = this.props;
      console.log("Back pressed", nav);
      const activeRoute = nav.routes[nav.index];
      if (activeRoute.index === 0) {
        return false;
      }
      dispatch(NavigationActions.back());
      return true;
    }

  
    render() {
      return (
          <ScreenStack ref={ref => NavigationService.setNavigator(ref)}/>
      )
    };
  }
  
export default AppNavigator;