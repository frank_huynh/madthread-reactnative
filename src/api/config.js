import config from './config';
import axios from 'axios';
import {ApolloClient} from 'apollo-client';
import {HttpLink} from 'apollo-link-http';
import {onError} from 'apollo-link-error'
import {InMemoryCache} from 'apollo-cache-inmemory';
import _ from 'lodash';
import 'rxjs'

const instance = axios.create({
    serverURL: `${config.serverURL}`
});

const httpLink = new HttpLink({
    uri: config.serverURL,
    headers: {

    }
});

const errorHandler = onError(({networkError}) => {
    switch (networkError.statusCode) {
        case 404:
            return {error: {message: 'Cannot connect to server'}};
        default:
            return {error: {message: 'Unknown Error'}};    
    }
});

const client = new ApolloClient({
    link: errorHandler.concat(httpLink),
    cache: new InMemoryCache()
});