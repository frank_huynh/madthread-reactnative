import { createStore, compose, applyMiddleware } from 'redux';
import app from './rootReducer';
import devTools from 'remote-redux-devtools';

import { createEpicMiddleware } from 'redux-observable';
import rootEpic from './rootEpic';
import { createLogger } from 'redux-logger';
import {createReactNavigationReduxMiddleware} from "react-navigation-redux-helpers";
import { persistStore, persistReducer } from 'redux-persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import storage from 'redux-persist/lib/storage'

const navMiddleware = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav,
);

const epicMiddleware = createEpicMiddleware();
let applyMiddlewares = applyMiddleware(epicMiddleware, navMiddleware);

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLevel2
}

const persistedReducer = persistReducer(persistConfig, app);

if (__DEV__) {
    const logger = createLogger({ collapsed: true });
    applyMiddlewares = applyMiddleware(epicMiddleware, navMiddleware, logger);
}

const enhancer = compose(
    applyMiddlewares,
    devTools(),
);

export default () => {
    let store = createStore(persistedReducer, enhancer);

    epicMiddleware.run(rootEpic);
    let persistor = persistStore(store);
    return {store, persistor}
};