import {StackNavigator} from 'react-navigation'
import OnBoarding from './screens/OnBoarding'
import Home from './screens/Home'

export const ScreenStack = StackNavigator({
    Root: {
        screen: OnBoarding,
        navigationOptions: {
            header: null
        }
    },
    Home: {
        screen: Home,
        navigationOptions: {
            header: null
        }
    }
})