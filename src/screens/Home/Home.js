import { View, Text, StyleSheet } from 'react-native';
import {colors} from '../../utils/themeConfig'
import React, {Component} from 'react';

export default class Home extends Component {

    constructor (props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Home. You can test your component here</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#64abab',
        justifyContent: 'center',
        alignItems: 'center'
    },
    styless: {
        width: 100,
        height: 100,
        backgroundColor: colors.mainWhite
    }
})