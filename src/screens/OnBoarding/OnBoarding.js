import React, {Component} from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {colors} from '../../utils/themeConfig'
import NavigationService from '../../NavigationService'

export default class OnBoarding extends Component {

    constructor (props) {
        super(props);
    }

    _navigateToHome = () => {
        NavigationService.navigate('Home', {});
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>OnBoarding</Text>
                <TouchableOpacity onPress={() => this._navigateToHome()}
                    style={styles.navigateButtonStyle}>
                    <Text>Press to go home</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#64abab',
        justifyContent: 'center',
        alignItems: 'center'
    },
    onBoardingTextStyle: {
        width: 100,
        height: 100,
        backgroundColor: colors.mainWhite
    },
    navigateButtonStyle: {
        marginTop: 100,
        backgroundColor: colors.mainWhite,
        paddingLeft: 100,
        paddingRight: 100,
        paddingTop: 20,
        paddingBottom: 20
    }
})